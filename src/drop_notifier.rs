use crate::sync::Arc;

use crate::notify::Notify;

pub(super) fn notifier() -> (DropNotifier, DropListener) {
    let notify = Arc::new(Notify::new());

    (
        DropNotifier {
            notify: Arc::clone(&notify),
        },
        DropListener { notify },
    )
}

pub(super) struct DropNotifier {
    notify: Arc<Notify>,
}

pub(super) struct DropListener {
    notify: Arc<Notify>,
}

impl DropListener {
    pub(super) async fn listen(self) {
        self.notify.listen().await.await
    }
}

impl Drop for DropNotifier {
    fn drop(&mut self) {
        self.notify.notify_one();
    }
}
