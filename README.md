# Async CPUPool

A simple async threadpool for CPU-bound tasks

## Usage

Add to your Cargo.toml
```bash
$ cargo add async-cpupool
```

Use in your application
```rust
fn complex_computation() {}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    smol::block_on(async {
        let pool = async_cpupool::CpuPool::new();

        pool.spawn(move || {
            complex_computation();
        }).await?;

        pool.close().await;

        Ok(())
    })
}
```

## Contributing
Feel free to open issues for anything you find an issue with. Please note that any contributed code will be licensed under the GPLv3.

## License

Copyright © 2023 asonix

Async CPUPool is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Async CPUPool is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. This file is part of Async CPUPool.

You should have received a copy of the GNU General Public License along with Async CPUPool. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
